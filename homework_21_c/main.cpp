//
//  main.cpp
//  homework_21_c
//
//  Created by Ilya Komrakov on 23.08.2020.
//  Copyright © 2020 Ilya Komrakov. All rights reserved.
//

#include <iostream>


// Abstract virtual class with a description of functionality
// This abstract class is an interface and its name must start with "I"
class IA
{
public:
    IA()
    {
        std::cout << "A constructed\n";
    }
    virtual ~IA() { }
    virtual void ShowInfo() = 0;
};


// Implementation method ShowInfo
void IA::ShowInfo()
{
    std::cout << "Default implementation\n";
}


// Virtual inheritance from class IA
class B : virtual public IA
{
public:
    B()
    {
        std::cout << "B constructed\n";
    }
    ~B() { }
    
    virtual void ShowInfo() override
    {
        IA::ShowInfo();
        std::cout << "Show info in class B\n";
    }
};


// Virtual inheritance from class IA
class C : virtual public IA
{
public:
    C()
    {
        std::cout << "C constructed\n";
    }
    ~C() { }
    
    virtual void ShowInfo() override
    {
        IA::ShowInfo();
    }
};


// Virtual inheritance from classes B and C
// Inheritance once from each parent class
class D : virtual public B, virtual public C
{
public:
    D()
    {
        std::cout << "D constructed\n";
    }
    ~D() { }
    
    virtual void ShowInfo() override
    {
        IA::ShowInfo();
    }
};





int main(int argc, const char * argv[]) {
    
    D* a = new D;
    delete a;
    
    return 0;
}
